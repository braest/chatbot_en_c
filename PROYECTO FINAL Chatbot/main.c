#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdbool.h>
#include <time.h>
#define ARRIBA    72
#define IZQUIERDA 75
#define DERECHA   77
#define ABAJO     80
#define ESC       27

int ser[1000][3];
int xx=10,yy=12,xc,yc,m;
int lar=4,dir=3,vel=150,pun=0;
int s=1,a=1;
char tec;

void gotoxy();
void color();
void instrucc();
void presentacion();
void marco();
void posicion();
void serpiente();
void borrador();
void controles();
void masvelocidad();
void comida();
bool perder();
void puntuacion();
void camposicion();
void finalizacion();

int main ()
{
	color(9,0);printf("\t\t\t\t\t\t\t CHATBOT version 0.1\n");
	color(15,0);gotoxy(2,2);printf("Pida a Dany jugar el juego del \"SNAKE\" con las sig. palabras claves:");
	gotoxy(8,3);printf("\"serpiente\", \"vibora\", \"snake\" y con la sig. sintaxis:");
	color(10,0);gotoxy(24,5);printf("Por favor -orden- Dany.");
	color(14,0);gotoxy(14,6);printf("Ejemplo: Por favor quiero jugar snake Dany.");
    color(15,0);gotoxy(55,20);printf("by -ESTRADA MARQUEZ");
    color(15,0);gotoxy(55,21);printf("   -GRANADOS SANTOYO");
	color(13,0);gotoxy(30,8);printf("Hola !!!");
    gotoxy(25,9);printf("Me llamo Daniela.\n ");
    color(11,0);gotoxy(14,11);

    FILE * flujo;
   char entrada [100];
   char respuesta[50][80];
   char juego[50][80] = {"serpiente", "vibora" , "snake"};
	int i ,d,j, tam = sizeof(juego) / sizeof(char *);

   flujo = fopen ("datos.txt" , "r");
   if (flujo == NULL) perror ("Error al intentar abrir el archivo");
   else {
     if ( fgets (entrada , 100 , stdin) != NULL )
            fclose (flujo);
   }

   //printf("%s\n",entrada );
   char* palabra;
   int q=0;
   palabra = strtok (entrada," ,.-\n");
  while (palabra != NULL)
  {
  		strcpy(respuesta[q],palabra);
  		q++;
     palabra = strtok (NULL, " ,.-");
  }

   //printf("%s\n", respuesta[0]);
   //printf("%s\n", juego[0]);

   for (i = 0; i < 10; ++i)
   {
   		for (j = 0; j < 10; ++j)
   		{
   			 d =  strcmp(respuesta[i],juego[j]);
   			 if (d==0)
   			 {
   			 	system("cls");
   			 	srand(time(NULL));
    marco();
    instrucc();
    presentacion();
    while(m!=2)
    {
    system("cls");
    gotoxy(0,0);printf("  ");
    marco();
    color(12,0);gotoxy(xc=(rand()%73)+4,yc=(rand()%19)+4);printf("%c",219);
    do
    {
        borrador();
        posicion();
        serpiente();
        Sleep(vel);
        comida();
        controles();
        controles();
        puntuacion();
        camposicion();
    }while(tec!=ESC && perder());
    system("cls");
    marco();
    puntuacion();
    finalizacion();
    color(15,0);
    gotoxy(32,17);printf("1. VOLVER A JUGAR");
    gotoxy(36,19);printf("2. SALIR");
    color(11,0);
    gotoxy(29,21);printf("SELECCIONE UNA OPCION: ");
    color(15,0);
    scanf("%d",&m);fflush(stdin);
    xx=10,yy=12;
    lar=4,dir=3,vel=150,pun=0;
    s=1,a=1;
    }
    system("pause>NULL");
    gotoxy(1,25);
    return 0;
   			 }
   		}
   }
  return 0;
}

void gotoxy(int x,int y)
{
    COORD coord;
    coord.X=x;
    coord.Y=y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
    CONSOLE_CURSOR_INFO cci;
	GetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cci);
	cci.bVisible = 0;
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cci);
}
void color(int c, int b){
  HANDLE  hConsole;
  hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  FlushConsoleInputBuffer(hConsole);
  SetConsoleTextAttribute(hConsole, c+16*b);
}
void instrucc()
{
    gotoxy(20,7);printf("       CONTROLES DEL JUEGO:       ");
    gotoxy(21,9);printf("TECLA ARRIBA=DIERECCION ARRIBA");
    gotoxy(21,10);printf("TECLA ABAJO=DIERECCION ABAJO");
    gotoxy(21,11);printf("TECLA DERECHA=DIERECCION DERECHA");
    gotoxy(21,12);printf("TECLA IZQUIERA=DIERECCION IZQUIERDA");
    gotoxy(21,18);printf("(PRESIONE UNA TECLA PARA CONTINUAR)");
    tec=getch();
    gotoxy(20,7);printf("                                  ");
    gotoxy(21,9);printf("                              ");
    gotoxy(21,10);printf("                            ");
    gotoxy(21,11);printf("                                ");
    gotoxy(21,12);printf("                                   ");
    gotoxy(21,18);printf("                                   ");

}
void presentacion()
{
    char inicio[15][70]=
{
"        #########  ####  ###  #########  ###   ###  #########         ",
"        ###        ##### ###  ###   ###  ###  ###   ###               ",
"        #########  ### #####  #########  #######    ######            ",
"              ###  ###  ####  ###   ###  ###  ###   ###               ",
"        #########  ###   ###  ###   ###  ###   ###  #########         ",
"                                                                      ",
"                 ##                                                   ",
"             #  # ###### ## #                                         ",
"              ######## ### ##                                         ",
"             #  # ### ### ###                                         ",
"                 ##       ## ### ### ### ### #                        ",
"                          # ### ### ### ### ##                        ",
"                           ### ### ### ### ###                        ",
"                                           ## ### ### ### ###         ",
"                                           # ### ### ### ###          ",
};
    color(10,0);
    for(int i=0; i<16; i++)
    {
        for(int j=0; j<71; j++)
        {
            if(inicio[i][j]=='#')
            {
                gotoxy(j+5,i+5);printf("%c",178);
            }
        }
    }
    color(15,0);
    gotoxy(19,21);printf("(PRESIONE UNA TECLA PARA INICIAR EL JUEGO)");
    color(12,0);
    gotoxy(14,13);printf("%c%c",219,219);
    gotoxy(14,14);printf("%c%c",219,219);
    tec=getch();
    gotoxy(19,21);printf("                                          ");
    gotoxy(14,13);printf("  ");
    gotoxy(14,14);printf("  ");
    for(int i=0; i<15; i++)
    {
        for(int j=0; j<71; j++)
        {
            if(inicio[i][j]=='#')
            {
                gotoxy(j+5,i+5);printf(" ");
            }
        }
    }
}
void marco()
{
   color(8,0);
   for(int i=2;i<78;i++)
   {
       gotoxy(i,3);printf("%c",178);
       gotoxy(i,23);printf("%c",178);
   }
   for(int i=4;i<23;i++)
   {
       gotoxy(2,i);printf("%c",178);
       gotoxy(77,i);printf("%c",178);
   }
   color(15,0);
   gotoxy(2,3);printf("%c",219);
   gotoxy(2,23);printf("%c",219);
   gotoxy(77,3);printf("%c",219);
   gotoxy(77,23);printf("%c",219);
}
void posicion()
{
    ser[s][0]=xx;
    ser[s][1]=yy;
    ser[0][0]=xx;
    ser[0][1]=yy;
    s++;
    if(s==lar)
        s=1;
}
void serpiente()
{
    color(10,0);
    for(int i=1;i<lar;i++)
    {
       gotoxy(ser[i][0],ser[i][1]);printf("%c",219);
    }
    gotoxy(0,0);printf("  ");
    gotoxy(0,0);printf("  ");
}
void borrador()
{
       gotoxy(ser[s][0],ser[s][1]);printf(" ");
}
void controles()
{
           if(kbhit())
        {
           tec=getch();
           switch(tec)
           {
        case ARRIBA:
            if(dir!=2)
               dir=1;
            break;
        case ABAJO:
            if(dir!=1)
               dir=2;
            break;
        case DERECHA:
            if(dir!=4)
               dir=3;
            break;
        case IZQUIERDA:
            if(dir!=3)
               dir=4;
            break;
           }
        }
}
void masvelocidad()
{
    if(pun==a*20)
    {
        vel-=10;
        a++;
    }
}
void comida()
{
    color(12,0);
    if(xx==xc && yy==yc)
    {
       xc=(rand()%73)+4;
       yc=(rand()%19)+4;
       lar++;
       pun+=10;
       if(xc==xx && yc==yy)
        {
            xc=(rand()%73)+4;
            yc=(rand()%19)+4;
            //gotoxy(xc,yc);printf("%c",219);
        };
       gotoxy(xc,yc);printf("%c",219);
       masvelocidad();
    }
}
bool perder()
{
  if( yy==3 || yy==23 || xx==2 || xx==77 )
    return false;
  for(int j=lar-1;j>0;j--)
  {
      if(ser[j][0]==xx && ser[j][1]==yy)
        return false;
  }
  return true;
}
void puntuacion()
{
    color(15,0);
    gotoxy(60,1);printf("PUNTUACI%cN %d",224,pun);
}
void camposicion()
{
    if(dir==1)yy--;
    if(dir==2)yy++;
    if(dir==3)xx++;
    if(dir==4)xx--;
}
void finalizacion()
{
    char inicio[11][70]=
{
"              #########  #########  #### ####  #########              ",
"              ###        ###   ###  #########  ###                    ",
"              ### #####  #########  ### # ###  ######                 ",
"              ###   ###  ###   ###  ###   ###  ###                    ",
"              #########  ###   ###  ###   ###  #########              ",
"                                                                      ",
"              #########  ###   ###  #########  ########               ",
"              ###   ###  ###   ###  ###        ###  ###               ",
"              ###   ###   ### ###   ######     #########              ",
"              ###   ###    #####    ###        ###   ###              ",
"              #########     ###     #########  ###   ###              ",

};
    color(13,0);
    for(int i=0; i<12; i++)
    {
        for(int j=0; j<71; j++)
        {
            if(inicio[i][j]=='#')
            {
                gotoxy(j+5,i+5);printf("%c",219);
            }
        }
    }
}

